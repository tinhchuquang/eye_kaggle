import pickle

import cv2
import imgaug as ia
import numpy as np
import pandas as pd
from imgaug import augmenters as iaa
from keras.applications import DenseNet121
from keras.layers import Input, GlobalAveragePooling2D, Dense, Dropout
from keras.models import Model
from sklearn.linear_model import LinearRegression

SIZE = 224
batch_size = 32
NUM_CLASSES = 1

sometimes = lambda aug: iaa.Sometimes(0.5, aug)
seq = iaa.Sequential(
    [
        # apply the following augmenters to most images
        iaa.Fliplr(0.5),  # horizontally flip 50% of all images
        iaa.Flipud(0.2),  # vertically flip 20% of all images
        sometimes(iaa.Affine(
            scale={"x": (0.9, 1.1), "y": (0.9, 1.1)},  # scale images to 80-120% of their size, individually per axis
            translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},  # translate by -20 to +20 percent (per axis)
            rotate=(-10, 10),  # rotate by -45 to +45 degrees
            shear=(-5, 5),  # shear by -16 to +16 degrees
            order=[0, 1],  # use nearest neighbour or bilinear interpolation (fast)
            cval=(0, 255),  # if mode is constant, use a cval between 0 and 255
            mode=ia.ALL  # use any of scikit-image's warping modes (see 2nd image from the top for examples)
        )),
        # execute 0 to 5 of the following (less important) augmenters per image
        # don't execute all of them, as that would often be way too strong
        iaa.SomeOf((0, 5),
                   [
                       sometimes(iaa.Superpixels(p_replace=(0, 1.0), n_segments=(20, 200))),
                       # convert images into their superpixel representation
                       iaa.OneOf([
                           iaa.GaussianBlur((0, 1.0)),  # blur images with a sigma between 0 and 3.0
                           iaa.AverageBlur(k=(3, 5)),  # blur image using local means with kernel sizes between 2 and 7
                           iaa.MedianBlur(k=(3, 5)),  # blur image using local medians with kernel sizes between 2 and 7
                       ]),
                       iaa.Sharpen(alpha=(0, 1.0), lightness=(0.9, 1.1)),  # sharpen images
                       iaa.Emboss(alpha=(0, 1.0), strength=(0, 2.0)),  # emboss images
                       # search either for all edges or for directed edges,
                       # blend the result with the original image using a blobby mask
                       iaa.SimplexNoiseAlpha(iaa.OneOf([
                           iaa.EdgeDetect(alpha=(0.5, 1.0)),
                           iaa.DirectedEdgeDetect(alpha=(0.5, 1.0), direction=(0.0, 1.0)),
                       ])),
                       iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.01 * 255), per_channel=0.5),
                       # add gaussian noise to images
                       iaa.OneOf([
                           iaa.Dropout((0.01, 0.05), per_channel=0.5),  # randomly remove up to 10% of the pixels
                           iaa.CoarseDropout((0.01, 0.03), size_percent=(0.01, 0.02), per_channel=0.2),
                       ]),
                       iaa.Invert(0.01, per_channel=True),  # invert color channels
                       iaa.Add((-2, 2), per_channel=0.5),
                       # change brightness of images (by -10 to 10 of original value)
                       iaa.AddToHueAndSaturation((-1, 1)),  # change hue and saturation
                       # either change the brightness of the whole image (sometimes
                       # per channel) or change the brightness of subareas
                       iaa.OneOf([
                           iaa.Multiply((0.9, 1.1), per_channel=0.5),
                           iaa.FrequencyNoiseAlpha(
                               exponent=(-1, 0),
                               first=iaa.Multiply((0.9, 1.1), per_channel=True),
                               second=iaa.ContrastNormalization((0.9, 1.1))
                           )
                       ]),
                       sometimes(iaa.ElasticTransformation(alpha=(0.5, 3.5), sigma=0.25)),
                       # move pixels locally around (with random strengths)
                       sometimes(iaa.PiecewiseAffine(scale=(0.01, 0.05))),  # sometimes move parts of the image around
                       sometimes(iaa.PerspectiveTransform(scale=(0.01, 0.1)))
                   ],
                   random_order=True
                   )
    ],
    random_order=True)


def create_model(input_shape, n_out):
    input_tensor = Input(input_shape)
    base_model = DenseNet121(include_top=False, weights='DenseNet-BC-121-32-no-top.h5',
                             input_tensor=input_tensor)
    X = GlobalAveragePooling2D()(base_model.output)
    X = Dropout(0.5)(X)
    X = Dense(1024, activation='relu')(X)
    X = Dropout(0.5)(X)
    final_output = Dense(n_out, activation='linear', name='final_output')(X)
    model = Model(input_tensor, final_output)
    return model


def train_generate(batch_x, batch_y, is_augment=True):
    batch_images = []
    for (sample, label) in zip(batch_x, batch_y):
        image = cv2.imread('/data/dungdv/KAGGLE_OLD_DATA/test/' + sample + '.jpeg')
        image = cv2.resize(image, (SIZE, SIZE))
        if is_augment:
            image = seq.augment_image(image)
        batch_images.append(image)
    batch_images = np.array(batch_images, np.float32) / 255
    batch_y = np.array(batch_y, np.float32)
    return batch_images, batch_y


def vaild_generate(batch_x, batch_y):
    batch_images = []
    for (sample, label) in zip(batch_x, batch_y):
        image = cv2.imread('/data/dungdv/KAGGLE/train_images/' + sample + '.png')
        image = cv2.resize(image, (SIZE, SIZE))
        batch_images.append(image)
    batch_images = np.array(batch_images, np.float32) / 255
    batch_y = np.array(batch_y, np.float32)
    return batch_images, batch_y


def load_weight(model_path='densenet_bestqwk.h5'):
    model = create_model(input_shape=(SIZE, SIZE, 3), n_out=NUM_CLASSES)
    model.load_weights(model_path)
    features = model.get_layer('dropout_2').output
    features_model = Model(inputs=model.input, outputs=[model.output, features])
    return features_model


df_train = pd.read_csv('/data/dungdv/KAGGLE_OLD_DATA/retinopathy_solution.csv')
df_test = pd.read_csv('/home/tinhcq/train.csv')

x = df_train['image']
y = df_train['level']

linear_regression = LinearRegression()
model = load_weight()
num_batch = len(x) // batch_size
num_epoch = 100
for _ in range(num_epoch):
    for batch in range(num_batch):
        top = batch * batch_size
        bot = np.min((batch + 1) * batch_size)
        batch_x, batch_y = train_generate(x[top:bot], y[top:bot], is_augment=True)
        y_pred = model.predict(batch_x)
        linear_regression.fit(y_pred[1][:], y)

pickle.dump(model, open('linear_regression', 'wb'))
