from efficientnet.keras import EfficientNetB7, EfficientNetB5
from keras import Input
from keras import Model
from keras.layers import BatchNormalization, Activation, Dropout
from keras.layers import Conv2DTranspose, GlobalAveragePooling2D
from keras.layers import UpSampling2D, Concatenate, Conv2D, Dense


def handle_block_names(stage):
    conv_name = 'decoder_state{}_conv'.format(stage)
    bn_name = 'decode_state{}_bn'.format(stage)
    relu_name = 'decode_state{}_relu'.format(stage)
    up_name = 'decode_state{}_upsample'.format(stage)
    return conv_name, bn_name, relu_name, up_name


def Upsample2D_block(filters, stage, kernel_size=(3, 3), upsample_rate=(2, 2),
                     batchnorm=False, skip=None):
    def layer(input_tensor):

        conv_name, bn_name, relu_name, up_name = handle_block_names(stage)

        x = UpSampling2D(size=upsample_rate, name=up_name)(input_tensor)

        if skip is not None:
            x = Concatenate()([x, skip])

        x = Conv2D(filters, kernel_size, padding='same', name=conv_name + '1')(x)
        if batchnorm:
            x = BatchNormalization(name=bn_name + '1')(x)
        x = Activation('relu', name=relu_name + '1')(x)

        x = Conv2D(filters, kernel_size, padding='same', name=conv_name + '2')(x)
        if batchnorm:
            x = BatchNormalization(name=bn_name + '2')(x)
        x = Activation('relu', name=relu_name + '2')(x)

        return x

    return layer


def Transpose2D_block(filters, stage, kernel_size=(3, 3), upsample_rate=(2, 2),
                      transpose_kernel_size=(4, 4), batchnorm=False, skip=None):
    def layer(input_tensor):

        conv_name, bn_name, relu_name, up_name = handle_block_names(stage)

        x = Conv2DTranspose(filters, transpose_kernel_size, strides=upsample_rate,
                            padding='same', name=up_name)(input_tensor)
        if batchnorm:
            x = BatchNormalization(name=bn_name + '1')(x)
        x = Activation('relu', name=relu_name + '1')(x)

        if skip is not None:
            x = Concatenate()([x, skip])

        x = Conv2D(filters, kernel_size, padding='same', name=conv_name + '2')(x)
        if batchnorm:
            x = BatchNormalization(name=bn_name + '2')(x)
        x = Activation('relu', name=relu_name + '2')(x)

        return x

    return layer


def build_unet(backbone, classes, last_block_filters, skip_layers,
               n_upsample_blocks=5, upsample_rates=(2, 2, 2, 2, 2),
               block_type='upsampling', activation='sigmoid',
               **kwargs):
    input = backbone.input
    x = backbone.output

    if block_type == 'transpose':
        up_block = Transpose2D_block
    else:
        up_block = Upsample2D_block

    # convert layer names to indices
    # skip_layers = ([get_layer_number(backbone, l) if isinstance(l, str) else l
    #                 for l in skip_layers])
    for i in range(n_upsample_blocks):

        # check if there is a skip connection
        if i < len(skip_layers):
            #             print(backbone.layers[skip_layers[i]])
            #             print(backbone.layers[skip_layers[i]].output)
            skip = backbone.layers[skip_layers[i]].output
        else:
            skip = None

        up_size = (upsample_rates[i], upsample_rates[i])
        filters = last_block_filters * 2 ** (n_upsample_blocks - (i + 1))

        x = up_block(filters, i, upsample_rate=up_size, skip=skip, **kwargs)(x)

    if classes < 2:
        activation = 'sigmoid'

    x = Conv2D(classes, (3, 3), padding='same', name='final_conv')(x)
    x = Activation(activation, name=activation)(x)

    model = Model(input, x)

    return model


def UEfficientNetB7(input_shape=(None, None, 3), n_out=1, final_features=3, decoder_filters=16,
                    decoder_block_type='upsampling',
                    encoder_weights=None, activation='sigmoid', **kwargs):
    input_tensor = Input(input_shape)
    backbone = EfficientNetB7(include_top=False, weights=encoder_weights, input_tensor=input_tensor)

    X = GlobalAveragePooling2D()(backbone.output)
    X = Dropout(0.5)(X)
    X = Dense(1024, activation='relu')(X)
    X = Dropout(0.5)(X)
    final_output = Dense(n_out, activation='linear', name='final_output')(X)

    skip_connections = list([554, 258, 155, 52])  # for resnet 34
    model = build_unet(backbone, final_features, decoder_filters,
                       skip_connections, block_type=decoder_block_type,
                       activation=activation, **kwargs)
    model = Model(backbone.input, [final_output, model.output])
    return model


def UEfficientNetB5(input_shape=(None, None, 3), n_out=1, final_features=3, decoder_filters=16,
                    decoder_block_type='upsampling',
                    encoder_weights=None, activation='sigmoid', **kwargs):
    input_tensor = Input(input_shape)
    backbone = EfficientNetB5(include_top=False, weights=encoder_weights, input_tensor=input_tensor)

    X = GlobalAveragePooling2D()(backbone.output)
    X = Dropout(0.5)(X)
    X = Dense(1024, activation='relu')(X)
    X = Dropout(0.5)(X)
    final_output = Dense(n_out, activation='linear', name='final_output')(X)

    skip_connections = list([391, 186, 113, 40])  # for resnet 34
    model = build_unet(backbone, final_features, decoder_filters,
                       skip_connections, block_type=decoder_block_type,
                       activation=activation, **kwargs)

    model = Model(backbone.input, [final_output, model.output])
    return model
