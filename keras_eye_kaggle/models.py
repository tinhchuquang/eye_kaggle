import tensorflow as tf
from keras import backend as K
from keras import layers
from keras.applications import DenseNet121
from keras.models import Model
from keras.models import Sequential
from keras.optimizers import Adam

densenet = DenseNet121(
    weights='../DATA/DenseNet-BC-121-32-no-top.h5',
    include_top=False,
    input_shape=(224, 224, 3)
)


def KerasFocalLoss(target, input):
    gamma = 2.
    input = tf.cast(input, tf.float32)

    max_val = K.clip(-input, 0, 1)
    loss = input - input * target + max_val + K.log(K.exp(-max_val) + K.exp(-input - max_val))
    invprobs = tf.log_sigmoid(-input * (target * 2.0 - 1.0))
    loss = K.exp(invprobs * gamma) * loss

    return K.mean(K.sum(loss, axis=1))


def build_model():
    model = Sequential()
    model.add(densenet)
    model.add(layers.GlobalAveragePooling2D())
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(5, activation='sigmoid'))

    model.compile(
        # loss='binary_crossentropy',
        loss=KerasFocalLoss,
        optimizer=Adam(lr=0.00005),
        metrics=['accuracy']
    )
    return model


def classification_model():
    X = densenet.output
    X = layers.GlobalAveragePooling2D()(X)
    X = layers.Dropout(0.5)(X)
    X = layers.Dense(5, activation='softmax')(X)
    model = Model(inputs=densenet.input, outputs=X)
    model.compile(
        loss='categorical_crossentropy',
        optimizer=Adam(lr=0.0001),
        metrics=['accuracy']
    )
    return model

# model = build_model()
# count = 1
# for layer in model.layers[1:]:
#     print(str(count) + "," + layer.name)
#     layers.trainable = False
#     count += 1
