import math

import cv2
import numpy as np
import pandas as pd
import tensorflow as tf
from keras.callbacks import Callback
from keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import cohen_kappa_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import quantile_transform
from tqdm import tqdm

from models import build_model

np.random.seed(2019)
tf.set_random_seed(2019)

train_df = pd.read_csv('../DATA/Eyes/train.csv')
IMAGE_SIZE = 224


# print(train_df.shape)
# train_df.head()

def get_pad_width(image, new_shape, is_rgb=True):
    pad_diff = new_shape - image.shape[0], new_shape - image.shape[1]
    t, b = math.floor(pad_diff[0] / 2), math.ceil(pad_diff[0] / 2)
    l, r = math.floor(pad_diff[1] / 2), math.ceil(pad_diff[1] / 2)
    if is_rgb:
        pad_width = ((t, b), (l, r), (0, 0))
    else:
        pad_width = ((t, b), (l, r))
    return pad_width


def crop_image(img, tol=7):
    w, h = img.shape[1], img.shape[0]
    gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    gray_img = cv2.blur(gray_img, (5, 5))
    shape = gray_img.shape
    gray_img = gray_img.reshape(-1, 1)
    quant = quantile_transform(gray_img, n_quantiles=256, random_state=0, copy=True)
    quant = (quant * 256).astype(int)
    gray_img = quant.reshape(shape)
    xp = (gray_img.mean(axis=0) > tol)
    yp = (gray_img.mean(axis=1) > tol)
    x1, x2 = np.argmax(xp), w - np.argmax(np.flip(xp))
    y1, y2 = np.argmax(yp), h - np.argmax(np.flip(yp))
    if x1 >= x2 or y1 >= y2:  # something wrong with the crop
        return img  # return original image
    else:
        img1 = img[y1:y2, x1:x2, 0]
        img2 = img[y1:y2, x1:x2, 1]
        img3 = img[y1:y2, x1:x2, 2]
        img = np.stack([img1, img2, img3], axis=-1)
    return img


def process_image(image, size=512):
    image = cv2.imread(image)
    image = cv2.resize(image, (size, int(size * image.shape[0] / image.shape[1])))
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    try:
        image = crop_image(image, tol=15)
    except Exception as e:
        image = image
        print(str(e))
    image = cv2.resize(image, (224, 224))
    return image


def info_image(im):
    # Compute the center (cx, cy) and radius of the eye
    cy = im.shape[0] // 2
    midline = im[cy, :]
    midline = np.where(midline > midline.mean() / 3)[0]
    if len(midline) > im.shape[1] // 2:
        x_start, x_end = np.min(midline), np.max(midline)
    else:  # This actually rarely happens p~1/10000
        x_start, x_end = im.shape[1] // 10, 9 * im.shape[1] // 10
    cx = (x_start + x_end) / 2
    r = (x_end - x_start) / 2
    return cx, cy, r


def resize_image(im, augmentation=True):
    # Crops, resizes and potentially augments the image to IMAGE_SIZE
    cx, cy, r = info_image(im)
    scaling = IMAGE_SIZE / (2 * r)
    rotation = 0
    if augmentation:
        scaling *= 1 + 0.3 * (np.random.rand() - 0.5)
        rotation = 360 * np.random.rand()
    M = cv2.getRotationMatrix2D((cx, cy), rotation, scaling)
    M[0, 2] -= cx - IMAGE_SIZE / 2
    M[1, 2] -= cy - IMAGE_SIZE / 2
    return cv2.warpAffine(im, M, (IMAGE_SIZE, IMAGE_SIZE))  # This is the most important line


def subtract_median_bg_image(im):
    k = np.max(im.shape) // 20 * 2 + 1
    bg = cv2.medianBlur(im, k)
    return cv2.addWeighted(im, 4, bg, -4, 128)


def subtract_gaussian_bg_image(im):
    k = np.max(im.shape) / 10
    bg = cv2.GaussianBlur(im, (0, 0), k)
    return cv2.addWeighted(im, 4, bg, -4, 128)


# def preprocess_image(image_path, desired_size=224):
#     image = Image.open(image_path)
#     image = image.resize((desired_size,) * 2, resample=Image.LANCZOS)
#     return image

def preprocess_image(path, resize=True, augmentation=False, subtract_gaussian=False, subtract_median=False):
    im = cv2.imread(path)
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    if resize:
        im = resize_image(im, augmentation)
    if subtract_gaussian:
        im = subtract_gaussian_bg_image(im)
    if subtract_median:
        im = subtract_median_bg_image(im)
    return im


def crop_image_from_gray(img, tol=7):
    if img.ndim == 2:
        mask = img > tol
        return img[np.ix_(mask.any(1), mask.any(0))]
    elif img.ndim == 3:
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        mask = gray_img > tol

        check_shape = img[:, :, 0][np.ix_(mask.any(1), mask.any(0))].shape[0]
        if (check_shape == 0):  # image is too dark so that we crop out everything,
            return img  # return original image
        else:
            img1 = img[:, :, 0][np.ix_(mask.any(1), mask.any(0))]
            img2 = img[:, :, 1][np.ix_(mask.any(1), mask.any(0))]
            img3 = img[:, :, 2][np.ix_(mask.any(1), mask.any(0))]
            #         print(img1.shape,img2.shape,img3.shape)
            img = np.stack([img1, img2, img3], axis=-1)
        #         print(img.shape)
        return img


def circle_crop(img, sigmaX=10):
    """
    Create circular crop around image centre
    """

    img = cv2.imread(img)
    img = crop_image_from_gray(img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    height, width, depth = img.shape

    x = int(width / 2)
    y = int(height / 2)
    r = np.amin((x, y))

    circle_img = np.zeros((height, width), np.uint8)
    cv2.circle(circle_img, (x, y), int(r), 1, thickness=-1)
    img = cv2.bitwise_and(img, img, mask=circle_img)
    img = crop_image_from_gray(img)
    img = cv2.addWeighted(img, 4, cv2.GaussianBlur(img, (0, 0), sigmaX), -4, 128)
    img = cv2.resize(img, (224, 224))
    return img


N = train_df.shape[0]
x_train = np.empty((N, 224, 224, 3), dtype=np.uint8)

for i, image_id in enumerate(tqdm(train_df['id_code'])):
    x_train[i, :, :, :] = circle_crop(
        f'/data/Data/Eyes/{image_id}.png'
    )

y_train = pd.get_dummies(train_df['diagnosis']).values
y_train_multi = np.empty(y_train.shape, dtype=y_train.dtype)
y_train_multi[:, 4] = y_train[:, 4]

for i in range(3, -1, -1):
    y_train_multi[:, i] = np.logical_or(y_train[:, i], y_train_multi[:, i + 1])

x_train, x_val, y_train, y_val = train_test_split(
    x_train, y_train_multi,
    test_size=0.2,
    random_state=2019
)
#
# class_weight = {0: 1.,
#                 1: 6.,
#                 2: 3.,
#                 3: 6.,
#                 4: 6.}

BATCH_SIZE = 32


def create_datagen():
    return ImageDataGenerator(
        zoom_range=0.15,  # set range for random zoom
        # set mode for filling points outside the input boundaries
        fill_mode='constant',
        rotation_range=120,
        cval=0.,  # value used for fill_mode = "constant"
        horizontal_flip=True,  # randomly flip images
        vertical_flip=True,  # randomly flip images
    )


# Using original generator
data_generator = create_datagen().flow(x_train, y_train, batch_size=BATCH_SIZE, seed=2019)


class Metrics(Callback):
    def on_train_begin(self, logs={}):
        self.val_kappas = []

    def on_epoch_end(self, epoch, logs={}):
        X_val, y_val = self.validation_data[:2]
        y_val = y_val.sum(axis=1) - 1

        y_pred = self.model.predict(X_val) > 0.5
        y_pred = y_pred.astype(int).sum(axis=1) - 1

        # y_pred = self.model.predict(X_val)
        # y_pred = y_pred.mean(axis=1)
        # y_pred[y_pred < 0.5] = 0
        # y_pred[(y_pred > 0.5) & (y_pred < 1.5)] = 1
        # y_pred[(y_pred > 1.5) & (y_pred < 2.5)] = 2
        # y_pred[(y_pred > 2.5) & (y_pred < 3.5)] = 3
        # y_pred[y_pred > 3.5] = 4

        _val_kappa = cohen_kappa_score(
            y_val,
            y_pred,
            weights='quadratic'
        )

        self.val_kappas.append(_val_kappa)

        print(f"val_kappa: {_val_kappa:.4f}")

        if _val_kappa == max(self.val_kappas):
            print("Validation Kappa has improved. Saving model.")
            self.model.save('model.h5')

        return


model = build_model()

kappa_metrics = Metrics()

history = model.fit_generator(
    data_generator,
    steps_per_epoch=x_train.shape[0] / BATCH_SIZE,
    epochs=30,
    validation_data=(x_val, y_val),
    callbacks=[kappa_metrics]
)
