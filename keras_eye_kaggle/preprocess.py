import cv2
import numpy as np
from sklearn.preprocessing import quantile_transform


def crop_image(img, tol=7):
    w, h = img.shape[1], img.shape[0]
    gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    gray_img = cv2.blur(gray_img, (5, 5))
    shape = gray_img.shape
    gray_img = gray_img.reshape(-1, 1)
    quant = quantile_transform(gray_img, n_quantiles=256, random_state=0, copy=True)
    quant = (quant * 256).astype(int)
    gray_img = quant.reshape(shape)
    xp = (gray_img.mean(axis=0) > tol)
    yp = (gray_img.mean(axis=1) > tol)
    x1, x2 = np.argmax(xp), w - np.argmax(np.flip(xp))
    y1, y2 = np.argmax(yp), h - np.argmax(np.flip(yp))
    if x1 >= x2 or y1 >= y2:  # something wrong with the crop
        return img  # return original image
    else:
        img1 = img[y1:y2, x1:x2, 0]
        img2 = img[y1:y2, x1:x2, 1]
        img3 = img[y1:y2, x1:x2, 2]
        img = np.stack([img1, img2, img3], axis=-1)
    return img


def process_image(image, size=512):
    image = cv2.resize(image, (size, int(size * image.shape[0] / image.shape[1])))
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    try:
        image = crop_image(image, tol=15)
    except Exception as e:
        image = image
        print(str(e))
    return image


image = cv2.imread('../DATA/Eyes/0a4e1a29ffff.png')
image = process_image(image)
cv2.imshow('image', image)
image = cv2.resize(image, (224, 224))
print(image.shape)
cv2.waitKey(0)
