import cv2
import imgaug as ia
import numpy as np
import pandas as pd
import tensorflow as tf
from efficientnet.keras import EfficientNetB7
from imgaug import augmenters as iaa
from keras import backend as K
from keras.callbacks import (ModelCheckpoint, EarlyStopping, ReduceLROnPlateau, CSVLogger)
from keras.layers import Input, GlobalAveragePooling2D, Dense, Dropout
from keras.models import Model
from keras.optimizers import Adam
from keras.utils import Sequence
from sklearn.metrics import cohen_kappa_score
from sklearn.utils import shuffle
from keras.utils import to_categorical

NUM_CLASSES = 1
SIZE = 224

df_train = pd.read_csv('/data/dungdv/KAGGLE_OLD_DATA/retinopathy_solution.csv')
df_test = pd.read_csv('/home/tinhcq/train.csv')

x = df_train['image']
y = df_train['level']

# y = to_categorical(y, num_classes=NUM_CLASSES)
# y_train_multi = np.empty(y.shape, dtype=y.dtype)
# y_train_multi[:, 4] = y[:, 4]
#
# for i in range(3, -1, -1):
#     y_train_multi[:, i] = np.logical_or(y[:, i], y_train_multi[:, i + 1])

# train_x, valid_x, train_y, valid_y = train_test_split(x, y_train_multi, test_size=0.15, stratify=y_train_multi,
#                                                      random_state=8)
train_x = x
train_y = y

x = df_test['id_code']
y = df_test['diagnosis']

# y = to_categorical(y, num_classes=NUM_CLASSES)
# y_train_multi = np.empty(y.shape, dtype=y.dtype)
# y_train_multi[:, 4] = y[:, 4]
#
# for i in range(3, -1, -1):
#     y_train_multi[:, i] = np.logical_or(y[:, i], y_train_multi[:, i + 1])

valid_x = x
valid_y = y


def KerasFocalLoss(target, input):
    gamma = 2.
    input = tf.cast(input, tf.float32)

    max_val = K.clip(-input, 0, 1)
    loss = input - input * target + max_val + K.log(K.exp(-max_val) + K.exp(-input - max_val))
    invprobs = tf.log_sigmoid(-input * (target * 2.0 - 1.0))
    loss = K.exp(invprobs * gamma) * loss

    return K.mean(K.sum(loss, axis=1))


sometimes = lambda aug: iaa.Sometimes(0.5, aug)
seq = iaa.Sequential(
    [
        # apply the following augmenters to most images
        iaa.Fliplr(0.5),  # horizontally flip 50% of all images
        iaa.Flipud(0.2),  # vertically flip 20% of all images
        sometimes(iaa.Affine(
            scale={"x": (0.9, 1.1), "y": (0.9, 1.1)},  # scale images to 80-120% of their size, individually per axis
            translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},  # translate by -20 to +20 percent (per axis)
            rotate=(-10, 10),  # rotate by -45 to +45 degrees
            shear=(-5, 5),  # shear by -16 to +16 degrees
            order=[0, 1],  # use nearest neighbour or bilinear interpolation (fast)
            cval=(0, 255),  # if mode is constant, use a cval between 0 and 255
            mode=ia.ALL  # use any of scikit-image's warping modes (see 2nd image from the top for examples)
        )),
        # execute 0 to 5 of the following (less important) augmenters per image
        # don't execute all of them, as that would often be way too strong
        iaa.SomeOf((0, 5),
                   [
                       sometimes(iaa.Superpixels(p_replace=(0, 1.0), n_segments=(20, 200))),
                       # convert images into their superpixel representation
                       iaa.OneOf([
                           iaa.GaussianBlur((0, 1.0)),  # blur images with a sigma between 0 and 3.0
                           iaa.AverageBlur(k=(3, 5)),  # blur image using local means with kernel sizes between 2 and 7
                           iaa.MedianBlur(k=(3, 5)),  # blur image using local medians with kernel sizes between 2 and 7
                       ]),
                       iaa.Sharpen(alpha=(0, 1.0), lightness=(0.9, 1.1)),  # sharpen images
                       iaa.Emboss(alpha=(0, 1.0), strength=(0, 2.0)),  # emboss images
                       # search either for all edges or for directed edges,
                       # blend the result with the original image using a blobby mask
                       iaa.SimplexNoiseAlpha(iaa.OneOf([
                           iaa.EdgeDetect(alpha=(0.5, 1.0)),
                           iaa.DirectedEdgeDetect(alpha=(0.5, 1.0), direction=(0.0, 1.0)),
                       ])),
                       iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.01 * 255), per_channel=0.5),
                       # add gaussian noise to images
                       iaa.OneOf([
                           iaa.Dropout((0.01, 0.05), per_channel=0.5),  # randomly remove up to 10% of the pixels
                           iaa.CoarseDropout((0.01, 0.03), size_percent=(0.01, 0.02), per_channel=0.2),
                       ]),
                       iaa.Invert(0.01, per_channel=True),  # invert color channels
                       iaa.Add((-2, 2), per_channel=0.5),
                       # change brightness of images (by -10 to 10 of original value)
                       iaa.AddToHueAndSaturation((-1, 1)),  # change hue and saturation
                       # either change the brightness of the whole image (sometimes
                       # per channel) or change the brightness of subareas
                       iaa.OneOf([
                           iaa.Multiply((0.9, 1.1), per_channel=0.5),
                           iaa.FrequencyNoiseAlpha(
                               exponent=(-1, 0),
                               first=iaa.Multiply((0.9, 1.1), per_channel=True),
                               second=iaa.ContrastNormalization((0.9, 1.1))
                           )
                       ]),
                       sometimes(iaa.ElasticTransformation(alpha=(0.5, 3.5), sigma=0.25)),
                       # move pixels locally around (with random strengths)
                       sometimes(iaa.PiecewiseAffine(scale=(0.01, 0.05))),  # sometimes move parts of the image around
                       sometimes(iaa.PerspectiveTransform(scale=(0.01, 0.1)))
                   ],
                   random_order=True
                   )
    ],
    random_order=True)


class My_Generator(Sequence):
    def __init__(self, image_filenames, labels, batch_size, is_train=True, mix=False, augment=False):
        super(My_Generator, self).__init__()
        self.image_filenames, self.labels = image_filenames, labels
        self.batch_size = batch_size
        self.is_train = is_train
        self.is_augment = augment
        if self.is_train:
            self.on_epoch_end()
        self.is_mix = mix

    def __len__(self):
        return int(np.ceil(len(self.image_filenames) / float(self.batch_size)))

    def __getitem__(self, idx):
        batch_x = self.image_filenames[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.labels[idx * self.batch_size: (idx + 1) * self.batch_size]
        if self.is_train:
            return self.train_generate(batch_x, batch_y)
        return self.vaild_generate(batch_x, batch_y)

    def on_epoch_end(self):
        if self.is_train:
            self.image_filenames, self.labels = shuffle(self.image_filenames, self.labels)
        else:
            pass

    def mix_up(self, x, y):
        lam = np.random.beta(0.2, 0.4)
        origin_index = np.arange(int(len(x)))
        index_array = np.arange(int(len(y)))
        np.random.shuffle(index_array)

        mixed_x = lam * x[origin_index] + (1 - lam) * x[index_array]
        mixed_y = lam * y[origin_index] + (1 - lam) * y[index_array]
        return mixed_x, mixed_y

    def train_generate(self, batch_x, batch_y):
        batch_images = []
        for (sample, label) in zip(batch_x, batch_y):
            image = cv2.imread('/data/dungdv/KAGGLE_OLD_DATA/test/' + sample + '.jpeg')
            image = cv2.resize(image, (SIZE, SIZE))
            if self.is_augment:
                image = seq.augment_image(image)
            batch_images.append(image)
        batch_images = np.array(batch_images, np.float32) / 255
        class_batch_y = to_categorical(batch_y, num_classes=5)
        batch_y = np.array(batch_y, np.float32)
        if self.is_mix:
            batch_images, batch_y = self.mix_up(batch_images, batch_y)
        return batch_images, [batch_y, class_batch_y]

    def vaild_generate(self, batch_x, batch_y):
        batch_images = []
        for (sample, label) in zip(batch_x, batch_y):
            image = cv2.imread('/data/dungdv/KAGGLE/train_images/' + sample + '.png')
            image = cv2.resize(image, (SIZE, SIZE))
            if self.is_augment:
                image = seq.augment_image(image)
            batch_images.append(image)
        batch_images = np.array(batch_images, np.float32) / 255
        class_batch_y = to_categorical(batch_y, num_classes=5)
        batch_y = np.array(batch_y, np.float32)
        return batch_images, [batch_y, class_batch_y]

def create_model(input_shape, n_out):
    input_tensor = Input(input_shape)
    base_model = EfficientNetB7(include_top=False,
                                weights='efficientnet-b7_weights_tf_dim_ordering_tf_kernels_autoaugment_notop.h5',
                                input_tensor=input_tensor)
    X = GlobalAveragePooling2D()(base_model.output)
    X = Dropout(0.5)(X)
    X = Dense(1024, activation='relu')(X)
    X = Dropout(0.5)(X)
    final_output = Dense(n_out, activation='linear', name='final_output')(X)

    Y = GlobalAveragePooling2D()(base_model.output)
    Y = Dense(1024, activation='relu')(Y)
    class_final_output = Dense(5, activation='softmax', name='class_final_output')(Y)
    model = Model(input_tensor, [final_output, class_final_output])
    return model


epochs = 30
batch_size = 8
checkpoint = ModelCheckpoint('densenet_.h5', monitor='val_loss', verbose=1,
                             save_best_only=True, mode='min', save_weights_only=True)
reduceLROnPlat = ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=4,
                                   verbose=1, mode='auto', epsilon=0.0001)
early = EarlyStopping(monitor="val_loss",
                      mode="min",
                      patience=9)

csv_logger = CSVLogger(filename='training_log.csv',
                       separator=',',
                       append=True)

train_mixup = My_Generator(train_x, train_y, batch_size, is_train=True, mix=False, augment=True)
valid_mixup = My_Generator(valid_x, valid_y, batch_size, is_train=False, mix=False, augment=True)
valid_generator = My_Generator(valid_x, valid_y, batch_size, is_train=False)

model = create_model(
    input_shape=(SIZE, SIZE, 3),
    n_out=NUM_CLASSES)

from keras.callbacks import Callback


class QWKEvaluation(Callback):
    def __init__(self, validation_data=(), batch_size=64, interval=1, post_predfix='', coef=[0.5, 1.5, 2.5, 3.5]):
        super(Callback, self).__init__()

        self.interval = interval
        self.batch_size = batch_size
        self.valid_generator, self.y_val = validation_data
        self.history = []
        self.coef = coef
        self.post_predfix = post_predfix

    def on_epoch_end(self, epoch, logs={}):
        if epoch % self.interval == 0:
            y_pred = self.model.predict_generator(generator=self.valid_generator,
                                                  steps=np.ceil(float(len(self.y_val)) / float(self.batch_size)),
                                                  workers=1, use_multiprocessing=False,
                                                  verbose=1)

            def flatten(y):
                return np.argmax(y, axis=1).reshape(-1)

            def predict(y):
                y[y < self.coef[0]] = 0
                y[(self.coef[0] < y) & (y < self.coef[1])] = 1
                y[(self.coef[1] < y) & (y < self.coef[2])] = 2
                y[(self.coef[2] < y) & (y < self.coef[3])] = 3
                y[self.coef[3] < y] = 4
                return y

            score = cohen_kappa_score(self.y_val,
                                      predict(y_pred),
                                      labels=[0, 1, 2, 3, 4],
                                      weights='quadratic')
            print("\n epoch: %d - QWK_score: %.6f \n" % (epoch + 1, score))
            self.history.append(score)
            if score >= max(self.history):
                print('saving checkpoint: ', score)
                self.model.save('densenet_bestqwk_' + self.post_predfix + '.h5')



for layer in model.layers:
    layer.trainable = True
# model.compile(  # loss='categorical_crossentropy',
#     loss='mean_squared_error',
#     # loss=kappa_loss,
#     optimizer=Adam(lr=1e-4))
model.compile(  # loss='categorical_crossentropy',
    loss=['mean_squared_error', KerasFocalLoss],
    # loss=kappa_loss,
    optimizer=Adam(lr=1e-3))
qwk = QWKEvaluation(validation_data=(valid_generator, valid_y),
                    batch_size=batch_size, interval=1, post_predfix='')
callbacks_list = [checkpoint, csv_logger, reduceLROnPlat, early, qwk]
model.fit_generator(
    train_mixup,
    steps_per_epoch=np.ceil(float(len(train_x)) / float(batch_size)),
    validation_data=valid_generator,
    validation_steps=np.ceil(float(len(valid_x)) / float(batch_size)),
    epochs=epochs,
    verbose=1,
    workers=4, use_multiprocessing=False,
    callbacks=callbacks_list)

# Fit all data
print('Fit all data ... ')
model.compile(  # loss='categorical_crossentropy',
    loss=['mean_squared_error', KerasFocalLoss],
    # loss=kappa_loss,
    optimizer=Adam(lr=1e-4))
model.fit_generator(
    valid_mixup,
    steps_per_epoch=np.ceil(float(len(valid_x)) / float(batch_size)),
    validation_data=valid_generator,
    validation_steps=np.ceil(float(len(valid_x)) / float(batch_size)),
    epochs=epochs,
    verbose=1,
    workers=4, use_multiprocessing=False,
    callbacks=callbacks_list
)