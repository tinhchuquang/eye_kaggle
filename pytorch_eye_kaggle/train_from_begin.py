import os
import random
import time

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
from dataloader import MyDataset
from EfficientNet_PyTorch import EfficientNet
from optimizer import RAdam, Lookahead
from sklearn.metrics import cohen_kappa_score
from torch.utils.data import Dataset
from torchvision import transforms


def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True


batch_size = 8
num_classes = 1
seed_everything(1234)
lr = 1e-2

train_df = pd.read_csv('/data/dungdv/KAGGLE_OLD_DATA/retinopathy_solution.csv')
val_df = pd.read_csv('/home/tinhcq/train.csv')

train_df.reset_index(drop=True, inplace=True)
val_df.reset_index(drop=True, inplace=True)

train_transform = transforms.Compose([
    transforms.RandomHorizontalFlip(),
    transforms.RandomRotation((-120, 120)),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])

trainset = MyDataset(train_df, transform=train_transform)
train_loader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=4)
valset = MyDataset(val_df, False, transform=train_transform)
val_loader = torch.utils.data.DataLoader(valset, batch_size=batch_size, shuffle=False, num_workers=4)

model = EfficientNet.from_name('efficientnet-b5')
model.load_state_dict(torch.load('efficientnet-b5-b6417697.pth'))
in_features = model._fc.in_features
model._fc = nn.Linear(in_features, 1000)
model = nn.Sequential(model, nn.Dropout(0.5), nn.Linear(1000, 1))
model.cuda()

base_optimizer = RAdam(model.parameters(), lr=lr, weight_decay=1e-5)
optimizer = Lookahead(base_optimizer, k=5, alpha=0.5)
criterion = nn.MSELoss()
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.1)


def score_kappa(y_val, y_pred):
    coef = [0.5, 1.5, 2.5, 3.5]

    def predict(y):
        y[y < coef[0]] = 0
        y[(coef[0] < y) & (y < coef[1])] = 1
        y[(coef[1] < y) & (y < coef[2])] = 2
        y[(coef[2] < y) & (y < coef[3])] = 3
        y[coef[3] < y] = 4
        return y

    print(y_val.shape, y_pred.shape)
    score = cohen_kappa_score(y_val,
                              predict(y_pred),
                              labels=[0, 1, 2, 3, 4],
                              weights='quadratic')
    return score


best_avg_loss = 100.0
best_score = 0
n_epochs = 120


def train_model(epoch):
    model.train()

    avg_loss = 0.
    optimizer.zero_grad()
    for idx, (imgs, labels) in enumerate(train_loader):
        imgs_train, labels_train = imgs.cuda(), labels.float().cuda()
        output_train = model(imgs_train)
        loss = criterion(output_train, labels_train)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
        avg_loss += loss.item() / len(train_loader)
        if idx % 1000 == 99:
            print('Epoch {}/{} \t iterator {}/{} \t loss={:.4f} \t '.format(
                epoch + 1, n_epochs, idx, len(train_loader), avg_loss * len(train_loader) / (idx * batch_size)))

    return avg_loss


def test_model():
    avg_val_loss = 0.
    # test_preds = np.zeros((len(val_loader), 1))
    # test_labels = np.zeros((len(val_loader), 1))
    test_preds = []
    test_labels = []
    model.eval()
    with torch.no_grad():
        for idx, (imgs, labels) in enumerate(val_loader):
            imgs_vaild, labels_vaild = imgs.cuda(), labels.float().cuda()
            output_test = model(imgs_vaild)
            avg_val_loss += criterion(output_test, labels_vaild).item() / len(val_loader)
            if idx == 0:
                test_preds = output_test.cpu()
                test_labels = labels.cpu()
            else:
                test_preds = np.concatenate((test_preds, output_test.cpu()), axis=0)
                test_labels = np.concatenate((test_labels, labels.cpu()), axis=0)
        score = score_kappa(test_labels, test_preds)

    return avg_val_loss, score


for epoch in range(n_epochs):

    print('lr:', scheduler.get_lr()[0])
    start_time = time.time()
    avg_loss = train_model(epoch)
    avg_val_loss, score = test_model()
    elapsed_time = time.time() - start_time
    print('Epoch {}/{} \t loss={:.4f} \t val_loss={:.4f} \t kappa_score={:.2f} \t time={:.2f}s'.format(
        epoch + 1, n_epochs, avg_loss, avg_val_loss, score, elapsed_time))

    if avg_val_loss < best_avg_loss:
        best_avg_loss = avg_val_loss
        torch.save(model.state_dict(), 'efficientNet/weight_best.pth')

    if score > best_score:
        best_score = score
        torch.save(model.state_dict(), 'efficientNet/weight_best_score.pth')

    scheduler.step()
