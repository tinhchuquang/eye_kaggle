import cv2
import numpy as np
from torch.utils.data import Dataset
from torchvision import transforms

IMAGE_SIZE = 256
train_old_data = '/data/dungdv/KAGGLE_OLD_DATA/test/'
val_data = '/data/dungdv/KAGGLE/train_images/'


def crop_image_from_gray(img, tol=7):
    if img.ndim == 2:
        mask = img > tol
        return img[np.ix_(mask.any(1), mask.any(0))]
    elif img.ndim == 3:
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        mask = gray_img > tol

        check_shape = img[:, :, 0][np.ix_(mask.any(1), mask.any(0))].shape[0]
        if (check_shape == 0):  # image is too dark so that we crop out everything,
            return img  # return original image
        else:
            img1 = img[:, :, 0][np.ix_(mask.any(1), mask.any(0))]
            img2 = img[:, :, 1][np.ix_(mask.any(1), mask.any(0))]
            img3 = img[:, :, 2][np.ix_(mask.any(1), mask.any(0))]
            #         print(img1.shape,img2.shape,img3.shape)
            img = np.stack([img1, img2, img3], axis=-1)
        #         print(img.shape)
        return img


class MyDataset(Dataset):

    def __init__(self, dataframe, is_train=True, transform=None):
        self.df = dataframe
        self.is_train = is_train
        self.transform = transform

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        if self.is_train:
            label = self.df.level.values[idx]
            label = np.expand_dims(label, -1)
            p = self.df.image.values[idx]
            p_path = train_old_data + p + '.jpeg'

        else:
            label = self.df.diagnosis.values[idx]
            label = np.expand_dims(label, -1)
            p = self.df.id_code.values[idx]
            p_path = val_data + p + '.png'

        image = cv2.imread(p_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = crop_image_from_gray(image)
        image = cv2.resize(image, (IMAGE_SIZE, IMAGE_SIZE))
        image = cv2.addWeighted(image, 4, cv2.GaussianBlur(image, (0, 0), 30), -4, 128)
        image = transforms.ToPILImage()(image)

        if self.transform:
            image = self.transform(image)

        return image, label
